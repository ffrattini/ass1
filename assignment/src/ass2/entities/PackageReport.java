package ass2.entities;

import java.util.ArrayList;
import java.util.List;

public class PackageReport {

	private String packageName;

	private String fileName;

	private List<ClassReport> interfaces;

	private List<ClassReport> classes;

	public PackageReport() {
		super();
		this.classes = new ArrayList<>();
		this.interfaces = new ArrayList<>();
	}

	public void setName (String name) {
		this.packageName = name;
	}

	public void setPath(String path) {
		this.fileName = path;
	}

	public void addInterface(ClassReport interfaceReport) {
		this.interfaces.add(interfaceReport);
	}

	public List<ClassReport> getInterfaces() {
		return this.interfaces;
	}

	public void addClass(ClassReport clas) {
		this.classes.add(clas);
	}

	public List<ClassReport> getClasses() {
		return this.classes;
	}

	public String getFullClassName() {
		return this.packageName;
	}

	public String getSrcFullFileName() {
		return this.fileName;
	}



}
