package ass2.entities;

public class MethodInfo {

	private String name;

	private int beginLine;

	private int endLine;

	private String parent;

	public MethodInfo(String name, int beginLine, int endLine) {
		this.name = name;
		this.beginLine = beginLine;
		this.endLine = endLine;
	}

	public String getName() {
		return this.name;
	}
	public int getBeginLine() {
		return this.beginLine;
	}
	public int getEndLine() {
		return  this.endLine;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getParent() { //TODO ritorna nodo genitore
		return this.parent;
	}
		
}
