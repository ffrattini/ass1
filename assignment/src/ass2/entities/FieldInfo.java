package ass2.entities;

public class FieldInfo {
	private String name;
	private String fieldType;

	public FieldInfo(String name, String fieldType) {
		this.name = name;
		this.fieldType = fieldType;
	}


	public String getName() {
		return this.name;
	}
	public String getFieldTypeFullName() {
		return this.fieldType;
	}

}
