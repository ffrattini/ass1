package ass2.entities;

import java.util.ArrayList;
import java.util.List;

public class ClassReport {

	private String className;
	private String srcPath;
	private List<MethodInfo> methods;
	private List<FieldInfo> fields;

	public boolean isInterface = false;

	public boolean mainClass = false;

	public ClassReport() {
		this.methods = new ArrayList<>();
		this.fields = new ArrayList<>();
	}

	public void setClass (String name, List<MethodInfo> methodInfo, List<FieldInfo> fieldInfo) {
		this.className = name;
		this.methods = methodInfo;
		this.fields = fieldInfo;
	}

	public void setClassName(String name) {
		this.className = name;
	}
	public void setSrcPath(String path) {
		this.srcPath = path;
	}

	public void addMethod(MethodInfo method) {
		this.methods.add(method);
	}

	public void addField(FieldInfo field) {
		this.fields.add(field);
	}
	public String getFullClassName() {
		return this.className;
	}

	public List<MethodInfo> getMethodsInfo() {
		return this.methods;
	}

	public List<FieldInfo> getFieldsInfo() {
		return this.fields;
	}

	public void setInterface(boolean flag) {
		this.isInterface = flag;
	}

	public boolean isInterface() { return this.isInterface; }

	public void hasMain() { this.mainClass = true; }

	public boolean isMainClass() { return this.mainClass; }

	@Override
	public String toString() {
		String fields = "";
		for(FieldInfo f : this.fields)
			fields = fields.concat(f.getName() + ", ");
		String methods = "";
		for (MethodInfo m : this.methods)
			methods = methods.concat(m.getName() + ", ");
		return "ClassReport [className=" + className + ", isInterface=" + isInterface +
				", fields=" + fields + ", methods=" + methods + "]";
	}
}
