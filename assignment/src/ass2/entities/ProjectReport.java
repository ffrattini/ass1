package ass2.entities;

import java.util.ArrayList;
import java.util.List;

public class ProjectReport {

	private List<PackageReport> packages;
	private List<ClassReport> classes;

	private String mainClass;

	public ProjectReport() {
		this.classes = new ArrayList<>();
		this.packages = new ArrayList<>();
	}

	public String getMainClass() {
		return this.mainClass;
	}

	public void setMainClass(String name) {
		this.mainClass = name;
	}
	
	public List<ClassReport> getAllClasses() {
		return this.classes;
	}
	
	public ClassReport getClassReport(String fullClassName) {
		return this.classes.stream().filter(c -> c.getFullClassName().equals(fullClassName)).findFirst().get();
	}

	public List<PackageReport> getAllPackages() {
		return this.packages;
	}

	public void addClass(ClassReport element) {
		classes.add(element);
	}

	public  void addPackage(PackageReport elem) {
		packages.add(elem);
	}
}
