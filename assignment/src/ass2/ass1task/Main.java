package ass2.ass1task;

import ass2.ass1task.common.Chrono;

public class Main {

    public static void main(String args[]) throws Exception {

        System.out.println("BodySimulation with Tasks\nStart");
        Chrono chrono = new Chrono();
        chrono.start();
        /*
        *** Simulation Without GUI ***
         */
        Service service = new Service();

        /*
        *** Simulation With GUI ****
         */
        //SimulationView viewer = new SimulationView(620,620);
        //Service service = new Service(viewer);
        service.execute();
        chrono.stop();
        System.out.println(chrono.getTime() + " ms");

        System.exit(0);
    }

}