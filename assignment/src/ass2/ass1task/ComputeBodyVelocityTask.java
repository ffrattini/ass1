package ass2.ass1task;

import ass2.ass1task.common.Body;
import ass2.ass1task.common.V2d;

import java.util.List;
import java.util.concurrent.Callable;

public class ComputeBodyVelocityTask implements Callable<Body> {
	
	private int bodyIndex;
	private double dt;
	private List<Body> bodies;

	public ComputeBodyVelocityTask(int bodyIndex, List<Body> bodies, double dt) {
		this.bodyIndex = bodyIndex;
		this.dt = dt;
		this.bodies = bodies;
	}

	@Override
	public Body call() {
		
		Body b = this.bodies.get(bodyIndex);
		
		Body bodyCopy = new Body(b.getId(), b.getPos(), b.getVel(), b.getMass());
		
		/* compute total force on bodies */
		V2d totalForce = computeTotalForceOnBody(bodyCopy);
							
		/* compute instant acceleration */
		V2d acc = new V2d(totalForce).scalarMul(1.0 / bodyCopy.getMass());

		/* update velocity */
		bodyCopy.updateVelocity(acc, this.dt);
		
		return bodyCopy;

	}
	
	private V2d computeTotalForceOnBody(Body b) {

		V2d totalForce = new V2d(0, 0);

		/* compute total repulsive force */
		for (int j = 0; j < this.bodies.size(); j++) {
			Body otherBody = this.bodies.get(j);
			if (!b.equals(otherBody)) {
				try {
					V2d forceByOtherBody = b.computeRepulsiveForceBy(otherBody);
					totalForce.sum(forceByOtherBody);
				} catch (Exception ex) {
				}
			}
		}

		/* add friction force */
		totalForce.sum(b.getCurrentFrictionForce());

		return totalForce;
	}

}
