package ass2.ass1task;



import ass2.ass1task.common.Body;
import ass2.ass1task.common.Boundary;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class ComputeBodyPositionTask implements Callable<Body> {
	
	private Future<Body> body;
	private double dt;
	private Boundary bounds;

	public ComputeBodyPositionTask(Future<Body> res, double dt, Boundary bounds) {
		this.body = res;
		this.dt = dt;
		this.bounds = bounds;
	}

	@Override
	public Body call() {
		
		Body b;
		try {
			b = body.get();
			
			/* compute bodies new pos */
			b.updatePos(dt);

			/* check collisions with boundaries */
			b.checkAndSolveBoundaryCollision(bounds);		
			
			//this.println("Body = " + b + " vt = " + this.env.getVt());
			
			return b;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
	}

}
