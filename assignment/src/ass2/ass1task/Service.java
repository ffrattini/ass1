package ass2.ass1task;



import ass2.ass1task.common.*;

import java.util.*;
import java.util.concurrent.*;

public class Service {
    private int numTasks;

    private int nBodies = 1000;
    private int nSteps = 1000;
    private Optional<SimulationView> viewer = Optional.empty();
    private ExecutorService executor;
    private ArrayList<Body> bodies;

    /* boundary of the field */
    private Boundary bounds;

    /* virtual time */
    private double vt;

    /* virtual time step */
    double dt;
    private int poolSize = Runtime.getRuntime().availableProcessors() + 1;


    public Service(){
        this.bounds = new Boundary(-3.0, -3.0, 3.0, 3.0);
        this.bodies = new ArrayList<Body>();
        this.initializeBodies();
        this.executor = Executors.newFixedThreadPool(poolSize);
    }
    public Service(SimulationView view){
        this.bounds = new Boundary(-3.0, -3.0, 3.0, 3.0);
        this.bodies = new ArrayList<Body>();
        this.initializeBodies();
        this.executor = Executors.newFixedThreadPool(poolSize);
        this.viewer = Optional.of(view);
    }

    public void execute() {

        int iter = 1;
        while (iter < this.nSteps) {
             System.out.println("Iteration # " + iter);

                List<Future<Body>> velResults = new LinkedList<Future<Body>>();
                for (int bodyIndex = 0; bodyIndex < this.nBodies; bodyIndex++) {
                    try {
                        Future<Body> resVel = executor.submit(new ComputeBodyVelocityTask(bodyIndex,
                                Collections.unmodifiableList(this.bodies), this.dt));
                        velResults.add(resVel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                List<Future<Body>> posResults = new LinkedList<Future<Body>>();
                for(Future<Body> resVel: velResults) {
                    try {
                        Future<Body> resPos = executor.submit(new ComputeBodyPositionTask(resVel, this.dt, this.bounds));
                        posResults.add(resPos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                for(Future<Body> resPos: posResults) {
                    try {
                        this.bodies.set(posResults.indexOf(resPos), resPos.get());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                this.vt += this.dt;
                iter++;


            if (viewer.isPresent())
                    this.viewer.get().display(this.bodies, this.vt, iter, this.bounds);

        }
    }

    private void initializeBodies() {
        Random rand = new Random(System.currentTimeMillis());

        for (int i = 0; i < nBodies; i++) {
            double x = bounds.getX0() * 0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
            double y = bounds.getY0() * 0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), 10);
            this.bodies.add(b);
        }
    }

    private void log(String msg){
        System.out.println("[SERVICE] "+msg);
    }
}
