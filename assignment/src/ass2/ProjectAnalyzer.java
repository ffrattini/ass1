package ass2;

import ass2.entities.ClassReport;
import ass2.entities.InterfaceReport;
import ass2.entities.PackageReport;
import ass2.entities.ProjectReport;
import ass2.visitor.ClassCollector;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import io.vertx.core.*;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class ProjectAnalyzer extends AbstractVerticle {

	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	private Vertx vertx = Vertx.vertx();

	/**
	 *
	 * Async method to retrieve the report about a specific class,
	 * given the full path of the interface source file
	 * @param srcInterfacePath
	 * @return Future<ClassCollector>
	 */
	public Future<ClassReport> getInterfaceReport(String srcInterfacePath) throws InterruptedException {

		Future<ClassReport> fut = vertx.executeBlocking(promise -> {
			try {
				CompilationUnit cu = StaticJavaParser.parse(new File(srcInterfacePath));
				InterfaceReport interfaceReport = new InterfaceReport();
				interfaceReport.setSrcPath(srcInterfacePath);
				ClassCollector visitor = new ClassCollector();
				visitor.visit(cu, interfaceReport);

				if (interfaceReport.isInterface()) {
					promise.complete(interfaceReport);
				} else {
					promise.fail("File does not contain an Interface");
				}
			}  catch (Exception e) {
				e.printStackTrace();
				promise.fail(e);
			}
		});

		return fut;
	}

	/**
	 * Async method to retrieve the report about a specific class,
	 * given the full path of the class source file
	 *
	 * @param srcClassPath
	 * @return
	 */
	public Future<ClassReport> getClassReport(String srcClassPath) {
		return vertx.executeBlocking(promise -> {
			try {
				CompilationUnit cu = StaticJavaParser.parse(new File(srcClassPath));
				ClassCollector fullc = new ClassCollector();
				ClassReport report = new ClassReport();
				report.setSrcPath(srcClassPath);
				fullc.visit(cu, report);
				promise.complete(report);
			}  catch (Exception e) {
				e.printStackTrace();
				promise.fail(e);
			}
		});
	}

	/**
	 * Async method to retrieve the report about a package,
	 * given the full path of the package folder
	 *
	 * @param srcPackagePath
	 * @return
	 */
	public Future<PackageReport> getPackageReport(String srcPackagePath) {

		return vertx.executeBlocking(promise -> {
			PackageReport packageReport = new PackageReport();
			File pack = new File(srcPackagePath);
			packageReport.setName(srcPackagePath.replace("/", ".").replace("\\", "."));
			packageReport.setPath(srcPackagePath);
			List<Future> futures = new ArrayList<>();

			for (File file : pack.listFiles()) {
				if (file.getAbsolutePath().endsWith(".java")) {

					futures.add(getClassReport(file.getAbsolutePath())
							.onSuccess(rep -> {
								if (rep.isInterface()) {
									packageReport.addInterface(rep);
								} else {
									packageReport.addClass(rep);
								}})
							.onFailure((Throwable t) -> log("errore nella future " + t)));
				}
			}

			CompositeFuture fut = CompositeFuture.all(futures);
			fut.onSuccess(a -> promise.complete(packageReport))
					.onFailure(p -> p.printStackTrace());

		});
	}

	/**
	 * Async method to retrieve the report about a project
	 * given the full path of the project folder
	 *
	 * @param srcProjectFolderPath
	 * @return
	 */
	public Future<ProjectReport> getProjectReport(String srcProjectFolderPath) {

		return vertx.executeBlocking(promise -> {
			ProjectReport projectReport = new ProjectReport();
			File pack = new File(srcProjectFolderPath);
			List<Future> futures = new ArrayList<>();

			futures.add(getPackageReport(srcProjectFolderPath).onSuccess(rep -> {
				for(ClassReport cr : rep.getClasses()) {
					if(cr.isMainClass()) {
						projectReport.setMainClass(cr.getFullClassName());
					}
					projectReport.addClass(cr);
				}
				projectReport.addPackage(rep);
			}).onFailure(p -> p.printStackTrace()));

			List<File> projectDirs = getSubdirs(pack);

			for (File dir : projectDirs) {
				futures.add(getPackageReport(dir.getPath()).onSuccess(rep -> {
					for(ClassReport cr : rep.getClasses()) {
						if(cr.isMainClass()) {
							projectReport.setMainClass(cr.getFullClassName());
						}
						projectReport.addClass(cr);
					}
					projectReport.addPackage(rep);
				}).onFailure(p -> log("Failed second package")));
			}

			CompositeFuture fut = CompositeFuture.all(futures);
			fut.onSuccess(a -> promise.complete(projectReport));
		});
	}

	/**
	 * NOT IMPLEMENTED
	 * Async function that analyze a project given the full path of the project folder,
	 * executing the callback each time a project element is found
	 *
	 * @param srcProjectFolderName
	 * @param callback
	 */
	void analyzeProject(String srcProjectFolderName, Consumer<ProjectElem> callback) {
		try (Stream<Path> paths = Files.walk(Paths.get(srcProjectFolderName))) {
			paths
					.filter(Files::isRegularFile)
					.filter(f -> f.toString().endsWith(".java"))
					.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void log (String msg) {
		System.out.println("SYSTEM:\n " + msg);
	}

	List<File> getSubdirs(File file) {
		List<File> subdirs = Arrays.asList(file.listFiles(new FileFilter() {
			public boolean accept(File f) {
				return f.isDirectory();
			}
		}));
		subdirs = new ArrayList<File>(subdirs);

		List<File> deepSubdirs = new ArrayList<File>();
		for (File subdir : subdirs) {
			deepSubdirs.addAll(getSubdirs(subdir));
		}
		subdirs.addAll(deepSubdirs);
		return subdirs;
	}
}
