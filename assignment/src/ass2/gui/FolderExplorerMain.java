package ass2.gui;

import javax.swing.*;

public class FolderExplorerMain extends JFrame{
        FolderExplorerMain(String path)
        {
            super("Project Explorer");
            add(new Explorer(path),"Center");
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setSize(800,800);
            setVisible(true);
        }

        public static void main(String[] args)
        {
            new FolderExplorerMain(".");
        }
    }
