package ass2;

import ass2.entities.*;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;

import java.util.List;

public class Main {
    public static void main(String args[]) throws Exception {

        ProjectAnalyzer project = new ProjectAnalyzer();
        String path = "src/ass2/";

        System.out.println("Visitng project at path..." + path);
        Future<ProjectReport> resource = project.getProjectReport(path);
        resource
                .onSuccess((ProjectReport res) -> {
                    List<PackageReport> packageReports = res.getAllPackages();
                    for (PackageReport p : packageReports) {
                        System.out.println("Package Name: " + p.getFullClassName());
                        System.out.println("Path: " + p.getSrcFullFileName());
                        for (ClassReport i : p.getInterfaces()) {
                            System.out.println("\tInterface Name: " + i.getFullClassName());
                            System.out.println("\t\tFields List:");
                            if (i.getFieldsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (FieldInfo f : i.getFieldsInfo())
                                    System.out.println("\t\t\t" + f.getName() + "\ttype_ " + f.getFieldTypeFullName());
                            System.out.println("\t\tMethods List:");
                            if (i.getMethodsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (MethodInfo m : i.getMethodsInfo())
                                    System.out.println("\t\t\t" + m.getName() + "\tstarts at line_ " +
                                            m.getBeginLine() + " ends at line_ " + m.getEndLine());
                        }
                        for (ClassReport c : p.getClasses()) {
                            System.out.println("\tClass Name: " + c.getFullClassName());
                            System.out.println("\t\tFields List:");
                            if (c.getFieldsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (FieldInfo f : c.getFieldsInfo())
                                    System.out.println("\t\t\t" + f.getName() + "\ttype_ " + f.getFieldTypeFullName());
                            System.out.println("\t\tMethods List:");
                            if (c.getMethodsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (MethodInfo m : c.getMethodsInfo())
                                    System.out.println("\t\t\t" + m.getName() + "\tstarts at line_ " +
                                            m.getBeginLine() + " ends at line_ " + m.getEndLine());
                        }
                    }
                        })
                .onFailure((Throwable t) -> log("errore nella future " + t))
                .onComplete((AsyncResult<ProjectReport> res) -> {
                    log("completion..." + res.succeeded());
                    System.exit(0);
                });

    }

    private static void log (String msg) {
        System.out.println("THREAD MANAGER:  " + msg);
    }
}
