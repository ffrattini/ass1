package ass2.visitor;

import ass2.entities.ClassReport;
import ass2.entities.FieldInfo;
import ass2.entities.MethodInfo;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class ClassCollector extends VoidVisitorAdapter<ClassReport> {
    public void visit(ClassOrInterfaceDeclaration cd, ClassReport classOrInterfaceReport) {
        super.visit(cd, classOrInterfaceReport);
        classOrInterfaceReport.setClassName(cd.getNameAsString());

        classOrInterfaceReport.setInterface(cd.isInterface());

    }

    public void visit(FieldDeclaration fd, ClassReport classOrInterfaceReport) {
        super.visit(fd, classOrInterfaceReport);

        FieldInfo fieldInfo = new FieldInfo(fd.getVariable(0).getNameAsString(), fd.getVariable(0).getTypeAsString());
        classOrInterfaceReport.addField(fieldInfo);
    }

    public void visit(MethodDeclaration md, ClassReport classOrInterfaceReport) {
        super.visit(md, classOrInterfaceReport);

        MethodInfo report = new MethodInfo(md.getNameAsString(), md.getName().getBegin().get().line, md.getRange().get().end.line);
        report.setParent(classOrInterfaceReport.getFullClassName());
        classOrInterfaceReport.addMethod(report);

        if(md.getNameAsString().equals("main")) {
            classOrInterfaceReport.hasMain();
        }
    }

}
