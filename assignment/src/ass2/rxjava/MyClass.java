package ass2.rxjava;

import ass2.entities.*;

import javax.swing.*;
import java.util.List;

public class MyClass extends JFrame {

    MyClass(String path) {
        super("Project Explorer");
        add(new ObserverExplorer(path),"Center");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800,800);
        setVisible(true);
    }

    public static void main(String[] args) throws Exception {

        /*
        *****          *****
        ***** OPEN GUI *****
        *****          ******/

        //new MyClass(".");

        /*
         *****                      *****
         ***** EXPLORE THE ENTIRE   *****
         ***** PROJECT FOLDER      ******
         *****                      ******/

        FromCallable callable = new FromCallable();
        ProjectReport res = callable.getProjectReport("src/ass2");
        List<PackageReport> packageReports = res.getAllPackages();
                    for (PackageReport p : packageReports) {
                        System.out.println("Package Name: " + p.getFullClassName());
                        System.out.println("Path: " + p.getSrcFullFileName());
                        for (ClassReport i : p.getInterfaces()) {
                            System.out.println("\tInterface Name: " + i.getFullClassName());
                            System.out.println("\t\tFields List:");
                            if (i.getFieldsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (FieldInfo f : i.getFieldsInfo())
                                    System.out.println("\t\t\t" + f.getName() + "\ttype_ " + f.getFieldTypeFullName());
                            System.out.println("\t\tMethods List:");
                            if (i.getMethodsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (MethodInfo m : i.getMethodsInfo())
                                    System.out.println("\t\t\t" + m.getName() + "\tstarts at line_ " +
                                            m.getBeginLine() + " ends at line_ " + m.getEndLine());
                        }
                        for (ClassReport c : p.getClasses()) {
                            System.out.println("\tClass Name: " + c.getFullClassName());
                            System.out.println("\t\tFields List:");
                            if (c.getFieldsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (FieldInfo f : c.getFieldsInfo())
                                    System.out.println("\t\t\t" + f.getName() + "\ttype_ " + f.getFieldTypeFullName());
                            System.out.println("\t\tMethods List:");
                            if (c.getMethodsInfo().isEmpty())
                                System.out.println("\t\t\t[ ]");
                            else
                                for (MethodInfo m : c.getMethodsInfo())
                                    System.out.println("\t\t\t" + m.getName() + "\tstarts at line_ " +
                                            m.getBeginLine() + " ends at line_ " + m.getEndLine());
                        }
                    }

}

}
