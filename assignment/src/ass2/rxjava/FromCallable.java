package ass2.rxjava;

import ass2.visitor.ClassCollector;
import ass2.entities.ClassReport;
import ass2.entities.PackageReport;
import ass2.entities.ProjectReport;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import io.reactivex.rxjava3.core.*;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FromCallable {

    public ProjectReport getProjectReport(String srcFolderPath) {
        ProjectReport projectReport = new ProjectReport();

        Observable<ProjectReport> projectReportObservable = Observable.fromCallable(() -> {
            ProjectReport project = new ProjectReport();
            File pack = new File(srcFolderPath);
            List<PackageReport> futures = new ArrayList<>();
            PackageReport rep = getPackageReport(srcFolderPath);
            futures.add(rep);
            for(ClassReport cr : rep.getClasses()) {
                if(cr.isMainClass()) {
                    project.setMainClass(cr.getFullClassName());
                }
                project.addClass(cr);
            }
            project.addPackage(rep);

            List<File> projectDirs = getSubdirs(pack);

            for (File dir : projectDirs) {
                PackageReport packageReport = getPackageReport(dir.getPath());
                futures.add(packageReport);
                for (ClassReport cr : packageReport.getClasses()) {
                    if (cr.isMainClass()) {
                        project.setMainClass(cr.getFullClassName());
                    }
                    project.addClass(cr);
                }
                project.addPackage(packageReport);
            }
            return project;
        });
        projectReportObservable.subscribe(project -> {
            for(ClassReport c: project.getAllClasses())
                projectReport.addClass(c);
            for(PackageReport p : project.getAllPackages())
                projectReport.addPackage(p);
        });
        return projectReport;
    }

    private PackageReport getPackageReport(String srcPackagePath) {
        PackageReport report = new PackageReport();
        Observable<PackageReport> observable = Observable.fromCallable(() -> {
            PackageReport packageReport = new PackageReport();
            File pack = new File(srcPackagePath);
            packageReport.setName(srcPackagePath.replace("/", ".").replace("\\", "."));
            packageReport.setPath(srcPackagePath);

            for (File file : pack.listFiles()) {
                if (file.getAbsolutePath().endsWith(".java")) {
                    ClassReport classReport = getClassReport(file.getAbsolutePath());
                    if (classReport.isInterface())
                        packageReport.addInterface(classReport);
                    else
                        packageReport.addClass(classReport);
            }
        }
            return packageReport;
        });
        observable.subscribe(packageReport -> {
            report.setPath(packageReport.getSrcFullFileName());
            report.setName(packageReport.getFullClassName());
            for(ClassReport c : packageReport.getClasses())
                report.addClass(c);
            for(ClassReport i : packageReport.getInterfaces())
                report.addInterface(i);
        });
        return report;
    }


    public ClassReport getClassReport(String srcClassPath) {
        ClassReport report = new ClassReport();
        Observable<ClassReport> observable = Observable.fromCallable(() -> {
            CompilationUnit cu = StaticJavaParser.parse(new File(srcClassPath));
            ClassCollector fullc = new ClassCollector();
            ClassReport res = new ClassReport();
            fullc.visit(cu, res);
            return res;
        });
        observable.subscribe(res -> {
                    report.setClass(res.getFullClassName(), res.getMethodsInfo(), res.getFieldsInfo());
                });
        return report;
    }

    static private void log(String msg) {
        System.out.println("[ " + Thread.currentThread().getName() + "  ] " + msg);
    }

    List<File> getSubdirs(File file) {
        List<File> subdirs = Arrays.asList(file.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.isDirectory();
            }
        }));
        subdirs = new ArrayList<File>(subdirs);

        List<File> deepSubdirs = new ArrayList<File>();
        for (File subdir : subdirs) {
            deepSubdirs.addAll(getSubdirs(subdir));
        }
        subdirs.addAll(deepSubdirs);
        return subdirs;
    }
}
