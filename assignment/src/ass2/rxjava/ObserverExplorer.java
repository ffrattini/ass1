package ass2.rxjava;

import ass2.entities.ClassReport;
import ass2.entities.FieldInfo;
import ass2.entities.MethodInfo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class ObserverExplorer extends JPanel implements ActionListener {
    JTextField jtf;
    JTree tree;
    JTable jtb;
    JScrollPane jsp;
    JScrollPane jspTable;
    JTextArea text;

    String currDirectory=null;

    final String[] colHeads={"File Name","SIZE(in Bytes)"};
    String[][]data={{"",""}};

    DefaultTableModel tableModel;

    ObserverExplorer(String path)
    {
        jtf=new JTextField();
        text = new JTextArea();

        File temp=new File(path);
        DefaultMutableTreeNode top=createTree(temp);

        tree=new JTree(top);

        jsp=new JScrollPane(tree);
        jsp.setPreferredSize(new Dimension(250, 800));
        final String[] colHeads={"File Name","SIZE(in Bytes)"};
        String[][]data={{"",""}};
        tableModel = new DefaultTableModel(data, colHeads);
        jtb=new JTable(tableModel);
        jspTable=new JScrollPane(jtb);
        setLayout(new BorderLayout());
        add(jtf,BorderLayout.NORTH);
        add(jsp,BorderLayout.WEST);
        add(jspTable,BorderLayout.CENTER);

        tree.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseClicked(MouseEvent me)
                    {
                        doMouseClicked(me);
                    }
                });
        jtf.addActionListener(this);
    }

    public void actionPerformed(ActionEvent ev)
    {
        File temp=new File(jtf.getText());
        DefaultMutableTreeNode newtop=createTree(temp);
        if(newtop!=null)
            tree=new JTree(newtop);
        remove(jsp);
        jsp=new JScrollPane(tree);
        jsp.setPreferredSize(new Dimension(250, 800));
        setVisible(false);
        add(jsp,BorderLayout.WEST);
        tree.addMouseListener(
                new MouseAdapter()
                {
                    public void mouseClicked(MouseEvent me)
                    {
                        doMouseClicked(me);
                    }
                });

        setVisible(true);
    }

    DefaultMutableTreeNode createTree(File temp)
    {
        DefaultMutableTreeNode top=new DefaultMutableTreeNode(temp.getPath());
        if(!(temp.exists() && temp.isDirectory()))
            return top;

        fillTree(top,temp.getPath());

        return top;
    }

    void fillTree(DefaultMutableTreeNode root, String filename)
    {
        File temp=new File(filename);

        if(!(temp.exists() && temp.isDirectory()))
            return;
        File[] filelist=temp.listFiles();

        for(int i=0; i<filelist.length; i++)
        {
            if(!filelist[i].isDirectory())
                continue;
            final DefaultMutableTreeNode tempDmtn=new DefaultMutableTreeNode(filelist[i].getName());
            root.add(tempDmtn);
            final String newfilename=new String(filename+"\\"+filelist[i].getName());
            Thread t=new Thread()
            {
                public void run()
                {
                    fillTree(tempDmtn,newfilename);
                }
            };
            if(t==null)
            {System.out.println("no more thread allowed "+newfilename);return;}
            t.start();
        }
    }

    void doMouseClicked(MouseEvent me)
    {
        TreePath tp=tree.getPathForLocation(me.getX(),me.getY());
        //System.out.println(tree.getPathForLocation(me.getX(), me.getY()));

        if(tp==null) return;

        String s=tp.toString();
        s=s.replace("[","");
        s=s.replace("]","");
        s=s.replace(", ","\\");
        jtf.setText(s);
        showFiles(s);

    }

    void showFiles(String filename)
    {
        File temp=new File(filename);

        data=new String[][]{{"",""}};

        if(!temp.exists()) return;
        if(!temp.isDirectory()) return;

        File[] filelist=temp.listFiles();

        int fileCounter=0;
        data=new String[filelist.length][2];
        for(int i=0; i<filelist.length; i++)
        {
            if(filelist[i].isDirectory())
                continue;
            data[fileCounter][0]=new String(filelist[i].getName());
            data[fileCounter][1]=new String(filelist[i].length()+"");
            fileCounter++;
        }

        String dataTemp[][]=new String[fileCounter][2];
        for(int k=0; k<fileCounter; k++)
            dataTemp[k]=data[k];
        data=dataTemp;

        remove(jspTable);

        jtb=new JTable(data, colHeads);

        jtb.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                File[] temp = filelist;
                String path = temp[jtb.rowAtPoint(evt.getPoint())].getPath();
                path = path.replace(".\\", "");
                System.out.println(path);
                if(path.endsWith(".java"))
                    printReport(path);
            }
        });
        jtb.setEnabled(false);
        jspTable=new JScrollPane(jtb);
        setVisible(false);
        add(jspTable,BorderLayout.CENTER);

        setVisible(true);
    }

    private void printReport(String path) {
        JFrame frame = new JFrame();
        FromCallable observer = new FromCallable();
        ClassReport res = observer.getClassReport(path);
        text = new JTextArea();
        text.setEditable(false);
        //text.setText("Package: \n\t" + res.getPackageFather() + "\n");
        if(res.isInterface)
            text.setText("Nome interfaccia: \n\t" + res.getFullClassName() + "\n");
        else
            text.setText("Nome classe: \n\t" + res.getFullClassName() + "\n");
        text.append("\n\tElenco Metodi: \n");
        if(res.getMethodsInfo().isEmpty())
            text.append("\t\t[ ]\n");
        else
            for(MethodInfo m : res.getMethodsInfo())
                text.append(String.format("\t\t%-30s %s", m.getName(), "Da riga " + m.getBeginLine()
                        + " a riga " + m.getEndLine() +"\n"));
            text.append(("\n\tElenco Campi\n"));
            if(res.getFieldsInfo().isEmpty())
                text.append("\t\t[ ]\n");
            else
                for (FieldInfo f : res.getFieldsInfo())
                    text.append(String.format("\t\t%-30s %s\n", f.getName(), f.getFieldTypeFullName()));

        JScrollPane scroll = new JScrollPane(text);
        frame.add(scroll);
        frame.setSize(500, 350);
        frame.setVisible(true);
    }

    private static void log (String msg) {
        System.out.println("THREAD MANAGER:  " + msg);
    }
}
