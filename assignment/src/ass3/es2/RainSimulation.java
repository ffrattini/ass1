package ass3.es2;

import ass3.es2.common.Area;
import ass3.es2.gui.ApplicationGUI;
import ass3.es2.gui.Grid;
import java.util.ArrayList;
import java.util.concurrent.Executors;


public class RainSimulation {
    private ArrayList<Area> areas;
    private Grid grid;

    private ApplicationGUI gui;
    private int width;
    private int height;
    private int day;

    ArrayList<Integer> nSensors;

    public RainSimulation(int width, int height) {
        this.width = width;
        this.height = height;
        areas = new ArrayList<>();
        grid = new Grid(width, height);

        createAreas();
        startGui();
    }

    public void run() {
        day = 1;
        while (true) {
            for (Area a : areas) {
                a.start(day);
            }
            day++;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private void createAreas() {
        for (int i = 0; i < 6; i++) {
            int w = width / 3;
            areas.add(new Area(i, grid.getPanel(i+1), w, height / 2));
        }
        nSensors = new ArrayList<>();
        for(Area a : areas) {
            nSensors.add(a.getNSensors());
        }
    }

    private void startGui() {
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                gui = new ApplicationGUI(400, 400, areas);
            }
        });
    }

}
