package ass3.es2.common;

import ass3.ass1actor.common.P2d;

public class Sensor {

    private P2d pos;

    private int id;

    private int alertLevel;

    private boolean isOnEmergency;

    private int mmWater;

    private boolean isFailed;


    public Sensor(int id, P2d pos) {
        this.id = id;
        this.pos = pos;
        alertLevel = 0;
        isOnEmergency = false;
        mmWater = 0;
        isFailed = false;
    }

    public P2d getPos() {
        return pos;
    }

    public int getAlertLevel() {
        return alertLevel;
    }

    public void setEmergency() { isOnEmergency = true; }

    public void cancelEmergency() {
        isOnEmergency = false;
        isFailed = false;
        mmWater = 10;
    }

    public void fail() { isFailed = true; }

    public boolean hasFailed() { return isFailed; }
    public boolean isOnEmergency() { return isOnEmergency; }

    public void wet(int level) {
        this.mmWater += level;
        checkWaterLevel();
    }

    public void dry(int level) {
        this.mmWater -= level;
        if (mmWater < 0)
            mmWater = 0;
        checkWaterLevel();
    }

    public void increaseAlert() {
        if (alertLevel < 2)
            alertLevel++;
        if (alertLevel == 2)
            setEmergency();
    }

    public void decreaseAlert() {
        if (alertLevel > 0)
            alertLevel--;
    }

    public void checkWaterLevel() {
        switch (alertLevel) {
            case 0: {
                if (mmWater > 20)
                    increaseAlert();
                break;
            }
            case 1: {
                if (mmWater < 20)
                    decreaseAlert();
                else if (mmWater > 40)
                    increaseAlert();
                break;
            }
            case 2: {
                if (mmWater < 40)
                    decreaseAlert();
                else if(!isOnEmergency())
                    setEmergency();
                break;
            }
            default: {
                break;
            }
        }
    }
}
