package ass3.es2.common;

import ass3.ass1actor.common.Boundary;
import ass3.ass1actor.common.P2d;
import ass3.es2.gui.Grid;
import ass3.es2.rabbitmq.Client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class Area {

    private List<Sensor> sensors;
    private Grid.VisualiserPanel panel;

    private int id;

    private int day;
    Boundary boundary;
    final double BOUNDARY_X0 = -1.0;
    final double BOUNDARY_Y0 = -1.0;
    final double BOUNDARY_X1 = 1.0;
    final double BOUNDARY_Y1 = 1.0;

    private int width;
    private int height;

    private Client client;

    private boolean alarm;
    private int majority;
    private boolean underManagement;

    public Area(int id, Grid.VisualiserPanel view, int width, int height) {
        this.id = id;
        this.panel = view;
        this.width = width;
        this.height = height;
        boundary = new Boundary(BOUNDARY_X0, BOUNDARY_Y0, BOUNDARY_X1, BOUNDARY_Y1);
        day = 0;
        alarm = false;
        underManagement = false;
        sensors = new ArrayList<>();
        this.panel.setX(this.width);
        this.panel.setY(this.height);
        generateSensors();
        try {
            client = new Client();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void start(int day) {
        Random rand = new Random();
        int cont = 0;
        if (!underManagement) {
            for (Sensor s : sensors) {
                if (!s.hasFailed())
                    if (rand.nextInt(100) == 13) {//sensor has failed, now it's broken
                        s.fail();
                        majority = majority > 0 ? majority - 1 : majority;
                    } else {
                        int waterLevel = (int) (10 * (rand.nextDouble() * rand.nextInt(6)));
                        if (rand.nextBoolean() && waterLevel > 0)  //se esce 1 piove, altrimenti il terreno si asciuga
                            s.wet(waterLevel);
                        else
                            s.dry(waterLevel);
                        if (s.isOnEmergency())
                            cont++;
                    }
            }
            if ((cont > majority || isEveryoneFailed()) && !alarm) { //se ho più della maggioranza dei sensori in allarme o se sono tutti falliti
                alarm = true;
                signalAlarm(id);
            }
        }
        panel.display((ArrayList<Sensor>) sensors, day, alarm);
    }

    private boolean isEveryoneFailed() {
        for(Sensor s : sensors)
            if(!s.hasFailed())
                return false;
        return true;
    }

    private void signalAlarm(int zone) {
        String i_str = Integer.toString(zone);
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    client.call(i_str);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void reenterAlarm() {
        alarm = false;
        this.sensors.forEach(Sensor::cancelEmergency);
        majority = sensors.size() / 2;
        underManagement = false;
    }

    public void alarmManagement() {
        underManagement = true;
    }

    public int getNSensors() { return sensors.size(); }

    private void generateSensors() {
        Random rand;
        for (int i = 0; i < new Random().nextInt(3) + 1; i++) {
            rand = new Random();
            double x = boundary.getX0() + rand.nextDouble() * (boundary.getX1() - boundary.getX0());
            double y = boundary.getY0() + rand.nextDouble() * (boundary.getY1() - boundary.getY0());
            Sensor b = new Sensor(i, new P2d(x, y));
            sensors.add(b);
        }
        panel.display((ArrayList<Sensor>) sensors, day, alarm);
        majority = sensors.size() / 2;
    }

}
