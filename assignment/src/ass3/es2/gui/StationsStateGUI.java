package ass3.es2.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class StationsStateGUI extends JFrame {
    private static final long serialVersionUID = -6218820567019985015L;
    private ArrayList<Integer> stationsState;
    private ArrayList<JPanel> panels;

    private ArrayList<JLabel> labels;

    public StationsStateGUI(ApplicationGUI parent, int size) {
        setTitle("Stato Caserme");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 400);
        this.panels = new ArrayList<>();
        labels = new ArrayList<>();

        stationsState = new ArrayList<>();
        for(int i = 0; i < size; i++)
            stationsState.add(0);

        JPanel panel = new JPanel(new GridLayout(2,3));
        this.getContentPane().add(BorderLayout.CENTER,panel);

        int cont = 0;

        for (int i=0; i<2; i++){
            for (int j=0; j<3; j++){
                cont++;
                JPanel jp = new JPanel();
                JLabel label = new JLabel("Stazione " + cont);
                label.setFocusable(false);
                label.setBorder(new EmptyBorder(30, 30, 10, 30));
                jp.setBorder(BorderFactory.createLineBorder(Color.black, 1));
                jp.add(label);
                JLabel label1 = new JLabel("Libera");
                label1.setFocusable(false);
                label1.setBorder(new EmptyBorder(30, 30, 20, 30));
                jp.add(label1);
                labels.add(label1);
                panel.add(jp);
                panels.add(jp);
            }
        }

        JPanel panel1 = new JPanel();
        JButton backButton = new JButton(" < ");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.setVisible(true);
                setVisible(false);
            }
        });
        panel1.add(backButton);
        this.getContentPane().add(BorderLayout.SOUTH,panel1);
        this.setVisible(true);
    }

    private JLabel getLabel (int i) {
        return this.labels.get(i);
    }

    private void checkStations() {
        for (int i = 0; i< panels.size(); i++) {
            JLabel label = getLabel(i);
            switch(stationsState.get(i)) {
                case 0: {
                    label.setText("Libera");
                    panels.get(i).setBackground(Color.white);
                    break;
                }
                case 1: {
                    label.setText("Occupata");
                    panels.get(i).setBackground(Color.yellow);
                    break;
                }
                case 2: {
                    label.setText("In allarme");
                    panels.get(i).setBackground(new Color(255, 108, 107));
                    break;
                }
                default:
                    break;
            }
        }
    }

    public void setStationInAlarm(int index) {
        System.out.println("Caserma #" + (index + 1) + " in allarme");
        stationsState.set(index, 2);
        checkStations();}
    public void setStationOccupied(int index) {
        System.out.println("Caserma #" + (index + 1) + " allarme in gestione");
        stationsState.set(index, 1);
        checkStations();
    }

    public void setStationFree(int index) {
        System.out.println("Caserma #" + (index + 1) + " allarme terminato");
        stationsState.set(index, 0);
        checkStations();
    }

}
