package ass3.es2.gui;

import ass3.es2.common.Area;
import ass3.es2.rabbitmq.Server;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executors;

public class ApplicationGUI extends JFrame {

    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;

    private Server server;

    AreaStateGUI areaStateGUI;
    StationsStateGUI stationsStateGUI;

    NumberOfSensorsGUI numberOfSensorsGUI;

    AlarmsGUI alarmsGUI;

    private ArrayList<Integer> counters;

    private ArrayList<Area> areas;

    public ApplicationGUI(int width, int height, ArrayList<Area> areas) {
        setTitle("Pannello Principale");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            server = new Server(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        this.areas = areas;

        counters = new ArrayList<>();
        for(int i = 0; i < this.areas.size(); i++)
            counters.add(0);

        areaStateGUI = new AreaStateGUI(this);
        areaStateGUI.setVisible(false);

        stationsStateGUI = new StationsStateGUI(this, this.areas.size());
        stationsStateGUI.setVisible(false);

        ArrayList<Integer> nSensors = new ArrayList<>();
        areas.forEach(a -> nSensors.add(a.getNSensors()));

        numberOfSensorsGUI = new NumberOfSensorsGUI(this, nSensors);
        numberOfSensorsGUI.setVisible(false);

        alarmsGUI = new AlarmsGUI(this);
        alarmsGUI.setVisible(false);

        //construct components
        button1 = new JButton ("Visualizza stato zone");
        button2 = new JButton ("Visualizza stato caserme");
        button3 = new JButton ("Numero sensori per zona");
        button4 = new JButton ("Disabilita Allarmi");

        button1.setAlignmentX(Component.CENTER_ALIGNMENT);
        button2.setAlignmentX(Component.CENTER_ALIGNMENT);
        button3.setAlignmentX(Component.CENTER_ALIGNMENT);
        button4.setAlignmentX(Component.CENTER_ALIGNMENT);

        button1.setBorder(new EmptyBorder(20, 20, 20, 20));
        button2.setBorder(new EmptyBorder(20, 20, 20, 20));
        button3.setBorder(new EmptyBorder(20, 20, 20, 20));
        button4.setBorder(new EmptyBorder(20, 20, 20, 20));

        ActionListener al = (e)->{
            areaStateGUI.setVisible(true);
            setVisible(false);
        };

        ActionListener al1 = (e) -> {
            stationsStateGUI.setVisible(true);
            setVisible(false);
        };

        ActionListener al2 = (e) -> {
          numberOfSensorsGUI.setVisible(true);
          setVisible(false);
        };

        ActionListener al3 = (e) -> {
            alarmsGUI.setVisible(true);
            setVisible(false);
        };

        button1.addActionListener(al);
        button2.addActionListener(al1);
        button3.addActionListener(al2);
        button4.addActionListener(al3);

        JPanel panel = new JPanel();
        panel.add(Box.createRigidArea(new Dimension(0,20)));
        panel.add(button1);
        panel.add(Box.createRigidArea(new Dimension(0,20)));
        panel.add(button2);
        panel.add(Box.createRigidArea(new Dimension(0,20)));
        panel.add(button3);
        panel.add(Box.createRigidArea(new Dimension(0,20)));
        panel.add(button4);

        //adjust size and set layout
        setSize (width, height);
        setResizable(false);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        add(panel);

        setVisible(true);

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    server.run();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

    }

    //nuovo allarme da segnalare per una determinata zona
    public void signalAlarm(int index) {
        areaStateGUI.setZoneinAlarm(index);
        alarmsGUI.setAlarm(index);
        stationsStateGUI.setStationInAlarm(index);
    }

    //i pompieri stanno gestendo l'allarme per una determinata caserma
    public void alarmOnManagement(int index) {
        if (index != -1) {
            stationsStateGUI.setStationOccupied(index);
            areas.get(index).alarmManagement();
            //tempo necessario per disattivare allarme
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Random rand = new Random(System.currentTimeMillis());
                        Thread.sleep(rand.nextInt(12000) + 3000);
                        alarmEnded(index);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
    }

    public void alarmEnded(int index) {
        counters.set(index, counters.get(index) + 1);
        if(counters.get(index)==2) {
            stationsStateGUI.setStationFree(index);
            areaStateGUI.setZoneFree(index);
            counters.set(index, 0);
            areas.get(index).reenterAlarm();
        }
    }
}
