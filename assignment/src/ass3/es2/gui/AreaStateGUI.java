package ass3.es2.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AreaStateGUI extends JFrame  {
    private static final long serialVersionUID = -6218820567019985015L;

    private ArrayList<JLabel> labels;
    private ArrayList<JPanel> panels;

    public AreaStateGUI(ApplicationGUI parent) {
        setTitle("Stato Zone");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 400);
        panels = new ArrayList<>();
        labels = new ArrayList<>();

        JPanel panel = new JPanel(new GridLayout(2,3));
        this.getContentPane().add(BorderLayout.CENTER,panel);

        int cont = 0;

        for (int i=0; i<2; i++){
            for (int j=0; j<3; j++){
                cont++;
                JPanel jp = new JPanel();
                JLabel label = new JLabel("Zona " + cont);
                label.setSize(jp.getWidth(), 50);
                label.setFocusable(false);
                label.setBorder(new EmptyBorder(30, 30, 10, 30));
                jp.setBorder(BorderFactory.createLineBorder(Color.black, 1));
                jp.add(label);
                JLabel label1 = new JLabel("Nessun problema");
                label1.setFocusable(false);
                label1.setSize(jp.getWidth(), 50);
                label1.setBorder(new EmptyBorder(30, 30, 20, 30));
                jp.add(label1);
                panel.add(jp);
                labels.add(label1);
                panels.add(jp);
            }
        }

        JPanel panel1 = new JPanel();
        JButton backButton = new JButton(" < ");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.setVisible(true);
                setVisible(false);
            }
        });
        panel1.add(backButton);
        this.getContentPane().add(BorderLayout.SOUTH,panel1);
        this.setVisible(true);
    }

    //segnala allarme in un determinata zona
    public void setZoneinAlarm(int index) {
        panels.get(index).setBackground(new Color(255, 108, 107));
        getLabel(index).setText("In allarme");
    }

    //rimuove allarme in una determinata zona
    public void setZoneFree(int index) {
        panels.get(index).setBackground(Color.white);
        getLabel(index).setText("Nessuno problema");
    }

    private JLabel getLabel (int i) {
        return this.labels.get(i);
    }

}
