package ass3.es2.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class AlarmsGUI extends JFrame{
    private static final long serialVersionUID = -6218820567019985015L;
    private ArrayList<JPanel> panels;
    private ArrayList<JButton> buttons;

    public AlarmsGUI(ApplicationGUI parent) {
        setTitle("Gestione Allarmi");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 400);
        panels = new ArrayList<>();
        buttons = new ArrayList<>();

        JPanel panel = new JPanel(new GridLayout(2,3));
        this.getContentPane().add(BorderLayout.CENTER,panel);

        int cont = 0;

        for (int i=0; i<2; i++){
            for (int j=0; j<3; j++){
                cont++;
                JPanel jp = new JPanel();
                JLabel label = new JLabel("Zona " + cont);
                label.setSize(jp.getWidth(), 50);
                label.setFocusable(false);
                jp.setBorder(BorderFactory.createLineBorder(Color.black, 1));
                jp.add(Box.createRigidArea(new Dimension(0,50)));
                jp.add(label);
                jp.add(Box.createRigidArea(new Dimension(0,40)));
                panel.add(jp);
                panels.add(jp);
            }
        }

        for(JPanel p : panels) {
            JButton jb = new JButton();
            jb.setLayout(new BorderLayout());
            JLabel label1 = new JLabel("Disattiva");
            JLabel label2 = new JLabel(" allarme");
            jb.add(BorderLayout.NORTH,label1);
            jb.add(BorderLayout.SOUTH,label2);
            jb.setVisible(false);
            jb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    parent.alarmOnManagement(panels.indexOf(p));
                    p.setBackground(Color.white);
                    jb.setVisible(false);
                }
            });
            buttons.add(jb);
            p.add(jb, BorderLayout.SOUTH);
        }

        JPanel panel1 = new JPanel();
        JButton backButton = new JButton(" < ");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.setVisible(true);
                setVisible(false);
            }
        });
        panel1.add(backButton);
        this.getContentPane().add(BorderLayout.SOUTH,panel1);
        this.setVisible(true);
    }

    private JPanel getPanel(int i) {
        return this.panels.get(i);
    }

    private JButton getButton(int i) {
        return this.buttons.get(i);
    }

    public void setAlarm(int index) {
        JPanel p = getPanel(index);
        JButton button = getButton(index);
        button.setVisible(true);
        p.setBackground(new Color(255, 108, 107));
    }

}
