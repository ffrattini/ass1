package ass3.es2.gui;

import ass3.es2.common.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NumberOfSensorsGUI extends JFrame {
    private static final long serialVersionUID = -6218820567019985015L;
    private final Map<JButton, Pair<Integer,Integer>> buttons = new HashMap<>();

    private ArrayList<Integer> sensors;

    public NumberOfSensorsGUI(ApplicationGUI parent, ArrayList<Integer> number) {
        setTitle("Visualizzazione Sensori");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 400);
        this.sensors = number;

        JPanel panel = new JPanel(new GridLayout(2,3));
        this.getContentPane().add(BorderLayout.CENTER,panel);

        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            final Pair<Integer,Integer> pos = buttons.get(bt);
            int index = (pos.getX() == 1 ? pos.getX() + 2 : pos.getX()) + pos.getY();
            String message = bt.getText();
            bt.setText(String.valueOf(sensors.get(index)));
            bt.setEnabled(false);
        };

        int cont = 0;

        for (int i=0; i<2; i++){
            for (int j=0; j<3; j++){
                cont++;
                final JButton jb = new JButton("Zona " + cont);
                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<>(i,j));
                panel.add(jb);
            }
        }

        JPanel panel1 = new JPanel();
        JButton backButton = new JButton(" < ");
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.setVisible(true);
                setVisible(false);
            }
        });
        panel1.add(backButton);
        this.getContentPane().add(BorderLayout.SOUTH,panel1);
        this.setVisible(true);
    }

}
