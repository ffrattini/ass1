package ass3.es2.gui;

import ass3.ass1actor.common.P2d;
import ass3.es2.common.Sensor;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.util.ArrayList;

public class Grid extends JFrame {

    private VisualiserPanel mainPanel, panel1, panel2, panel3, panel4, panel5, panel6;

    public Grid(int width, int height) {
        create(width, height);
    }

    public VisualiserPanel getPanel(int index) {
        switch (index) {
            case 1: {
                return panel1;
            }
            case 2: {
                return panel2;
            }
            case 3: {
                return panel3;
            }
            case 4: {
                return panel4;
            }
            case 5: {
                return panel5;
            }
            case 6: {
                return panel6;
            }
            default: {
                return null;
            }
        }
    }

    public void create(int width, int height) {
        setTitle("Rain Sensors");
        mainPanel = new VisualiserPanel(); // main panel
        mainPanel.setLayout(new GridLayout(2, 3));
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        panel1 = new VisualiserPanel(); // sub-panel 1
        panel1.setBackground(new Color(224, 227, 246));
        panel1.setColor(new Color(224, 227, 246));
        panel2 = new VisualiserPanel();
        panel2.setBackground(new Color(225, 227, 226));
        panel2.setColor(new Color(225, 227, 226));
        panel3 = new VisualiserPanel();
        panel3.setBackground(new Color(219, 255, 248));
        panel3.setColor(new Color(219, 255, 248));
        panel4 = new VisualiserPanel();
        panel4.setBackground(new Color(252, 220, 220));
        panel4.setColor(new Color(252, 220, 220));
        panel5 = new VisualiserPanel();
        panel5.setBackground(new Color(218, 254, 222));
        panel5.setColor(new Color(218, 254, 222));
        panel6 = new VisualiserPanel();
        panel6.setBackground(new Color(255, 247, 223));
        panel6.setColor(new Color(255, 247, 223));
        mainPanel.add(panel1);
        mainPanel.add(panel2);
        mainPanel.add(panel3);
        mainPanel.add(panel4);
        mainPanel.add(panel5);
        mainPanel.add(panel6);
        add(mainPanel);
        setSize(width, height);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void display(ArrayList<Sensor> sensors, int day, boolean alarm){
        try {
            SwingUtilities.invokeAndWait(() -> {
                mainPanel.display(sensors,day, alarm);
                panel1.display(sensors,day, alarm);
                panel2.display(sensors,day, alarm);
                panel3.display(sensors,day, alarm);
                panel4.display(sensors,day, alarm);
                panel5.display(sensors,day, alarm);
                repaint();
            });
        } catch (Exception ex) {}
    };

    public static class VisualiserPanel extends JPanel {

        private ArrayList<Sensor> sensors;
        private int day;

        private int dx;
        private int dy;

        private Color color;

        private boolean alarm;

        public VisualiserPanel(){
        }

        public void setX(int width) {
            dx = width / 2 - 20;
        }

        public void setY(int height) {
            dy = height / 2 - 20;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (sensors != null) {
                Graphics2D g2 = (Graphics2D) g;
                if(alarm)
                    setBackground(new Color(255, 108, 107));
                else
                    setBackground(color);

                g2.drawString("Sensors: " + sensors.size() + " - day: " + day, dx + 20, dy * 2 + 20);

                sensors.forEach( s -> {
                    P2d p = s.getPos();
                    int radius = 10;
                    if(s.hasFailed())
                        g2.setColor(new Color(152, 153, 151));
                    else
                        switch (s.getAlertLevel()) {
                            case 0: {
                                g2.setColor(Color.GREEN);
                                break;
                            }
                            case 1: {
                                g2.setColor(Color.YELLOW);
                                break;
                            }
                            case 2: {
                                g2.setColor(Color.RED);
                                break;
                            }
                            default: {
                                g2.setColor(Color.GREEN);
                            }
                        }

                    g2.fillOval(getXcoord(p.getX()),getYcoord(p.getY()), radius, radius);
                    g2.setColor(Color.BLACK);
                    g2.drawOval(getXcoord(p.getX()),getYcoord(p.getY()), radius, radius);
                });
            }
        }

        private int getXcoord(double x) {
            return (int)(dx + x*dx);
        }

        private int getYcoord(double y) {
            return (int)(dy - y*dy);
        }

        public void display(ArrayList<Sensor> sensors, int days, boolean alarm){
            this.sensors = sensors;
            this.day = days;
            this.alarm = alarm;

            repaint();
        }

        public void setColor(Color c) { this.color = c; }
    }
}
