package ass3.ass1actor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import ass3.ass1actor.actors.ArbiterActor;
import ass3.ass1actor.common.Boundary;
import ass3.ass1actor.common.SimulationLogger;
import ass3.ass1actor.common.SimulationView;
import ass3.ass1actor.messages.*;

import java.util.ArrayList;
import java.util.Optional;

public class Controller {

    public enum SimulationState {
        READY,
        RUNNING,
        COMPLETED,
        CANCELED
    }

    static final int N_BODIES = 5000;
    static final int N_ITERATIONS = 10000;

    private static final double BOUNDARY_X0 = -1.0;
    private static final double BOUNDARY_Y0 = -1.0;
    private static final double BOUNDARY_X1 = 1.0;
    private static final double BOUNDARY_Y1 = 1.0;

    private final int nThreads;

    private Optional<SimulationView> viewer;
    private Optional<SimulationLogger> logger;

    private SimulationState simulationState;

    private double vt;

    private Boundary boundary;

    private ActorSystem system;

    private ActorRef arbiter;

    public Controller() {
        viewer = Optional.empty();
        logger = Optional.empty();
        simulationState = SimulationState.READY;
        vt = 0;
        boundary = new Boundary(BOUNDARY_X0, BOUNDARY_Y0, BOUNDARY_X1, BOUNDARY_Y1);

        int systemThreads = Runtime.getRuntime().availableProcessors();
        int programThreads = Math.max(3, (int)Math.round(systemThreads * 1.125));
        nThreads = Math.min(N_BODIES, programThreads - 2);

        system = ActorSystem.create("bodiesSimulation");
        arbiter = system.actorOf(Props.create(ArbiterActor.class), "Arbiter");
    }

    public void start() {

        simulationState = SimulationState.RUNNING;
        notifyLogger();

        arbiter.tell(new SimulationStartedMsg(nThreads, N_ITERATIONS, viewer, vt, N_BODIES, boundary), ActorRef.noSender());

        notifyLogger();
        clearViewer();

    }

    public void setViewer(SimulationView viewer) {
        this.viewer = Optional.of(viewer);
    }

    /*
     * Set a logger for the simulation
     * @param logger
     */
    public void setLogger(SimulationLogger logger) {
        this.logger = Optional.of(logger);
    }

    // Notify the logger about a state change
    private void notifyLogger() {
        if (logger.isPresent()) {
            logger.get().update(simulationState);
        }
    }

    public synchronized SimulationState getSimulationState() {
        return simulationState;
    }

    // Clear the viewer's content
    private void clearViewer() {
        if (viewer.isPresent()) {
            viewer.get().display(new ArrayList<>(), 0, 0, boundary);
        }
    }

    public synchronized void cancelSimulation() {
        simulationState = SimulationState.COMPLETED;
        arbiter.tell(new SimulationStopMsg(), ActorRef.noSender());
    }
}
