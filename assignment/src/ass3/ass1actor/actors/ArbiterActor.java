package ass3.ass1actor.actors;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import ass3.ass1actor.common.*;
import ass3.ass1actor.messages.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class ArbiterActor extends AbstractTerminableActor {

    private static final double DT = 0.01;
    private int N_ITERATIONS;

    private ArrayList<Body> list;
    private ActorSystem system;

    private List<ActorRef> actors;
    private int nBodies;

    private int iteration;

    private int nActors;
    int answers;

    private double vt;

    private Optional<SimulationView> viewer;
    private Boundary boundary;

    private Chrono chrono;

    @Override
    public Receive createReceive() {
        return noSimulationBehaviour();
    }

    private Receive noSimulationBehaviour() {
        return receiveBuilder()
                .match(SimulationStartedMsg.class, this::onSimulationStartMsg)
                .matchAny(msg -> onAny())
                .build();
    }

    private Receive waitForAnswerBehaviour() {
        return receiveBuilder()
                .match(FirstReturnMsg.class, this::continueWithIteration)
                .match(SecondReturnMsg.class, this::endIteration)
                .match(SimulationStopMsg.class, this::onSimulationStopMsg)
                .matchAny(msg -> onAny())
                .build();
    }

    private void onSimulationStopMsg(SimulationStopMsg msg) {
        actors.forEach(actor -> actor.tell(msg, getSelf()));
        terminate();
    }

    private void onSimulationStartMsg(SimulationStartedMsg msg) {
        chrono = new Chrono();
        chrono.start();
        system = ActorSystem.create("Supervisor");
        actors = new ArrayList<>();
        int nThreads = msg.getNThreads();
        N_ITERATIONS = msg.getIterations();
        nBodies = msg.getNBodies();
        vt = msg.getVt();
        viewer = msg.getViewer();
        boundary = msg.getBoundary();
        list = new ArrayList<>();
        createBody();

        int nBodiesPerWorker = nBodies / nThreads;
        int remainingBodies = nBodies % nThreads;
        int q = 0;
        //crea lista di attori e gli assegna una lista di corpi, poi li inizializza
        for (int i = 0, j = 0; i < nThreads; i++, j+=q) {
            q = i >= remainingBodies ? nBodiesPerWorker : nBodiesPerWorker + 1;
            ActorRef actor = system.actorOf(Props.create(BodiesActor.class), "Actor" + i);
            actors.add(actor);
            actor.tell(new SetupActorMsg(j, j + q -1, actor.path().name(), boundary), getSelf());
        }
        nActors = actors.size();
        iteration = 0;
        getContext().become(waitForAnswerBehaviour());
        startLoop();
    }

    private void startLoop() {
        if (iteration <= N_ITERATIONS) {
            updateViewer();
            answers = 0;
            iteration++;
            actors.forEach(actor -> actor.tell(new IterationMsg(list), getSelf()));
            getContext().become(waitForAnswerBehaviour());
        } else {
            if (viewer.isPresent()) {
                viewer.get().display(new ArrayList<>(), vt, iteration-1, boundary);
            }
            actors.forEach(actor -> actor.tell(new SimulationStopMsg(), getSelf()));
            chrono.stop();
            System.out.println("tempo impiegato :"+ chrono.getTime() + "ms");
            terminate();
        }
    }

    private void continueWithIteration(FirstReturnMsg msg) {
        answers++;
        if (answers == nActors) {
            answers = 0;
            list = msg.getBodies();
            actors.forEach(actor -> actor.tell(new CheckCollisionMsg(list), ActorRef.noSender()));
        }
    }

    private void endIteration(SecondReturnMsg msg) {
        answers++;
        if (answers==nActors) {
            answers = 0;
            vt = vt + DT;
            list = msg.getBodies();
            startLoop();
        }
    }

    private void onAny() { }

    private void updateViewer() {
        if (viewer.isPresent()) {
            viewer.get().display(list, vt, iteration, boundary);
        }
    }

    private void createBody() {
        Random rand = new Random(System.currentTimeMillis());
        for (int i = 0; i < nBodies; i++) {
            double x = boundary.getX0() + rand.nextDouble() * (boundary.getX1() - boundary.getX0());
            double y = boundary.getY0() + rand.nextDouble() * (boundary.getY1() - boundary.getY0());
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), 10);
            list.add(b);
        }
    }
}
