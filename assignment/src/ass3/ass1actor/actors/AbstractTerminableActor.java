package ass3.ass1actor.actors;

import akka.actor.AbstractActor;

/**
 * Extension of AbstractActor with an additional terminate() method
 */
public abstract class AbstractTerminableActor extends AbstractActor {

    /**
     * Terminate the actor
     */
    protected void terminate() {
        System.out.println(getSelf().path().name() + " > Terminating...");
        getContext().stop(getSelf());
    }

}
