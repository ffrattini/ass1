package ass3.ass1actor.actors;

import akka.actor.ActorRef;

import ass3.ass1actor.common.Body;
import ass3.ass1actor.common.Boundary;
import ass3.ass1actor.common.V2d;
import ass3.ass1actor.messages.*;

import java.util.ArrayList;


/**
 * Computer player of a Mastermind match
 */
public class BodiesActor extends AbstractTerminableActor {

    private ArrayList<Body> list;

    private int start;
    private int stop;

    private String name;

    private ActorRef arbiter;

    private final double dt = 0.01;

    private Boundary boundary;

    @Override
    public Receive createReceive() {
        return noSimulationBehaviour();
    }

    // Actor behaviour for when simulation isn't started yet
    private Receive noSimulationBehaviour() {
        return receiveBuilder()
                .match(SetupActorMsg.class, this::onSetupActorMessage)
                .matchAny(msg -> onAny())
                .build();
    }

    private void onSetupActorMessage(SetupActorMsg msg) {
        arbiter = getSender();
        start = msg.getStart();
        stop = msg.getStop();
        name = msg.getName();
        boundary = msg.getBoundary();

        getContext().become(stepBehaviour());
    }

    // Actor behaviour for when the player is waiting for its turn
    private Receive stepBehaviour() {
        return receiveBuilder()
                .match(SimulationStopMsg.class, this::onSimulationStopMsg)
                .match(IterationMsg.class, this::onIterationMsg)
                .matchAny(msg -> onAny())
                .build();
    }

    // Actor behaviour for when the player is waiting for the arbiter response to its message
    private Receive collisionBehaviour() {
        return receiveBuilder()
                .match(SimulationStopMsg.class, this::onSimulationStopMsg)
                .match(CheckCollisionMsg.class, this::onCheckCollisionMsg)
                .matchAny(msg -> onAny())
                .build();
    }

    private void onSimulationStopMsg(SimulationStopMsg msg) {
        terminate();
    }

    private void onIterationMsg(IterationMsg msg) {

        list = msg.getBodies();
        System.out.println(name + ": updating position");

        for (int i = start; i <= stop; i++) {
            Body b = list.get(i);
            /* compute total force on bodies */
            V2d totalForce = computeTotalForceOnBody(b);

            /* compute instant acceleration */
            V2d acc = new V2d(totalForce).scalarMul(1.0 / b.getMass());

            /* update velocity */
            b.updateVelocity(acc, dt);
        }

        // Update position
        for (int i = start; i <= stop; i++) {
            list.get(i).updatePos(dt);
        }
        arbiter.tell(new FirstReturnMsg(list), getSelf());

        getContext().become(collisionBehaviour());
    }

    // Handler for any unexpected message
    private void onAny() { }

    // Handler for GameStartedMsg message
    private void onCheckCollisionMsg(CheckCollisionMsg msg) {
        System.out.println(name + " checking collisions");
        list = msg.getBodies();
        // check collisions with other bodies
        for (int i = start; i <= stop; i++) {
            list.get(i).checkAndSolveBoundaryCollision(boundary);
        }
        arbiter.tell(new SecondReturnMsg(list), getSelf());
        getContext().become(stepBehaviour());
    }

    public V2d computeTotalForceOnBody(Body b) {

        V2d totalForce = new V2d(0, 0);

        /* compute total repulsive force */

        for (int j = 0; j < list.size(); j++) {
            Body otherBody = list.get(j);
            if (!b.equals(otherBody)) {
                try {
                    V2d forceByOtherBody = b.computeRepulsiveForceBy(otherBody);
                    totalForce.sum(forceByOtherBody);
                } catch (Exception ex) {
                }
            }
        }

        /* add friction force */
        totalForce.sum(b.getCurrentFrictionForce());

        return totalForce;
    }


}
