package ass3.ass1actor;

import ass3.ass1actor.common.SimulationLogger;
import ass3.ass1actor.common.Chrono;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ControlPanel extends JFrame implements SimulationLogger, ActionListener {
        private static final String BUTTON_TEXT_RESET = "Stop";
        private static final String BUTTON_TEXT_INFO = "Info";

        private static final long serialVersionUID = 1L;

        private final JButton stopButton;
        private final JButton showInfoButton;
        private final JTextArea logArea;
        private final Chrono chrono;

        private final Controller simulator;

        public ControlPanel(Controller simulator) {
            chrono = new Chrono();

            this.simulator = simulator;

            setTitle("Simulation Control Panel");
            setSize(300, 250);
            setResizable(false);
            addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent ev) {
                    System.exit(-1);
                }
                public void windowClosed(WindowEvent ev) {
                    System.exit(-1);
                }
            });

            stopButton = new JButton(BUTTON_TEXT_RESET);
            showInfoButton = new JButton(BUTTON_TEXT_INFO);
            logArea = new JTextArea();
            logArea.setEditable(false);
            JScrollPane scrollPane = new JScrollPane(logArea);
            JPanel btnPanel = new JPanel();
            btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
            btnPanel.add(stopButton);
            btnPanel.add(showInfoButton);
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout(5, 5));
            panel.add(btnPanel, BorderLayout.SOUTH);
            panel.add(scrollPane, BorderLayout.CENTER);
            getContentPane().add(panel);
            stopButton.addActionListener(this);
            showInfoButton.addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent ev) {
            Object src = ev.getSource();
            if (src == stopButton) {
                stopButton.setEnabled(false);
                simulator.cancelSimulation();
                appendLogText("  State: " + simulator.getSimulationState() + "\n  Time: " + chrono.getTime() + "ms");
            }  else {
                appendLogText("  State: " + simulator.getSimulationState() + "\n  Time: " + chrono.getTime() + "ms");
            }
        }

        // Append text to the log
        private void appendLogText(String text) {
            logArea.setText(logArea.getText() + text + "\n");
        }

        // Act based on the given simulation state
        public void update(Controller.SimulationState simulationState) {
            long time;
            switch (simulationState) {
                case RUNNING:
                    chrono.start();
                    SwingUtilities.invokeLater(() -> {
                        appendLogText("START");
                    });
                    break;
                case COMPLETED:
                    time = chrono.getTime();
                    SwingUtilities.invokeLater(() -> {
                        appendLogText("DONE\n  Simulation completed in " + time + "ms");
                    });
                    chrono.reset();
                    break;
                case CANCELED:
                    time = chrono.getTime();
                    SwingUtilities.invokeLater(() -> {
                        appendLogText("STOP\n  Simulation canceled after " + time + "ms");
                    });
                    chrono.reset();
                    break;
                default:
                    break;
            }
        }

    }


