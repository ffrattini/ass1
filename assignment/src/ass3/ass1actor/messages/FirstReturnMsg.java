package ass3.ass1actor.messages;

import ass3.ass1actor.common.Body;

import java.util.ArrayList;
import java.util.List;

public class FirstReturnMsg {
    private ArrayList<Body> bodies;


    public FirstReturnMsg(ArrayList<Body> list) {
        this.bodies = list;
    }


    public ArrayList<Body> getBodies() {
        return bodies;
    }
}
