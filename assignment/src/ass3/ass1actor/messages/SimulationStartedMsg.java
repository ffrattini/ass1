package ass3.ass1actor.messages;

import ass3.ass1actor.common.Boundary;
import ass3.ass1actor.common.SimulationView;

import java.util.Optional;

public class SimulationStartedMsg {
    private int nThreads;

    private int iterations;

    private final Optional<SimulationView> viewer;
    private final double vt;

    private final int nBodies;
    private Boundary boundary;

    public SimulationStartedMsg(int threads, int it, Optional<SimulationView> view, double vt, int nBodies,
                                Boundary boundary) {
        this.nThreads = threads;
        this.iterations = it;
        this.viewer = view;
        this.vt = vt;
        this.nBodies = nBodies;
        this.boundary = boundary;
    }

    public int getNThreads() {
        return nThreads;
    }

    public int getIterations() {
            return iterations;
        }

    public Optional<SimulationView> getViewer() {
        return viewer;
    }

    public double getVt() {
        return vt;
    }

    public int getNBodies() {
        return nBodies;
    }

    public Boundary getBoundary() {
        return boundary;
    }
}
