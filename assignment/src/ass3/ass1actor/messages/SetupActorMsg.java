package ass3.ass1actor.messages;

import ass3.ass1actor.common.Boundary;

import java.util.List;

public class SetupActorMsg {

    private final int start;
    private final int stop;

    private String name;
    private Boundary boundary;

    public SetupActorMsg(int start, int stop, String name, Boundary bounds) {
        this.start = start;
        this.stop = stop;
        this.name = name;
        this.boundary = bounds;
    }

    public int getStart() {
        return start;
    }

    public int getStop() {
        return stop;
    }

    public String getName() {
        return name;
    }

    public Boundary getBoundary() {
        return boundary;
    }
}
