package ass3.ass1actor.common;

import ass3.ass1actor.Controller;

/**
 * Represents a logger for the Simulator to write information when the simulation state changes
 */
public interface SimulationLogger {
    /**
     * Act based on the new simulation state
     * @param simulationState
     */
    void update(Controller.SimulationState simulationState);
}
