package ass3.ass1actor;

import ass3.ass1actor.common.SimulationView;

import javax.swing.*;

public class ActorBodySimulationMain {

    public static void main(String[] args) {
        Controller simulator = new Controller();
        System.out.println("Start");

        /***                     **
         * SIMULATION WITH GUI  **
         **                     **/
//        SimulationView viewer = new SimulationView(620,620);
//        ControlPanel controlPanel = new ControlPanel(simulator);
//        simulator.setViewer(viewer);
//        simulator.setLogger(controlPanel);
//        SwingUtilities.invokeLater(()-> {
//            controlPanel.setVisible(true);
//        });

        /***                        **
         *  SIMULATION WITHOUT GUI  **
         ***                        */

        simulator.start();


    }
}
