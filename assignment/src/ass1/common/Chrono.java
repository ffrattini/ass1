package ass1.common;

/**
 * Chronometer that can be paused and resumed
 */
public class Chrono {
    private boolean running;
    private long startTime;
    private long totalTime;

    /**
     * Constructor for the Chrono class
     */
    public Chrono(){
        reset();
    }
    /**
     * Start or resume the chronometer
     */
    public void start() {
        running = true;
        startTime = System.currentTimeMillis();
    }

    /**
     * Pause the chronometer
     */
    public void stop() {
        totalTime += getPartialTime();
        running = false;
        startTime = 0;
    }

    /**
     * Reset the chronometer
     */
    public void reset() {
        running = false;
        startTime = 0;
        totalTime = 0;
    }
    // Return the amount time passed since the last chronometer start
    private long getPartialTime() {
        return System.currentTimeMillis() - startTime;
    }

    /**
     * @return total elapsed time
     */
    public long getTime() {
        if (running) {
            return 	System.currentTimeMillis() - startTime + totalTime;
        } else {
            return totalTime;
        }
    }
}
