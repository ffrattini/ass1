package ass1.common;
import ass1.conc.Simulator.SimulationState;

/**
 * Represents a logger for the Simulator to write information when the simulation state changes
 */
public interface SimulationLogger {
    /**
     * Act based on the new simulation state
     * @param simulationState
     */
    void update(SimulationState simulationState);
}
