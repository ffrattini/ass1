package ass1.conc;

import ass1.common.*;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

/*
        * Simulates the movement of N bodies (subject to elastic hits) on a two-dimensional plane
        */
public class Simulator extends Thread {

    public enum SimulationState {
        READY,
        RUNNING,
        PAUSED,
        COMPLETED,
        CANCELED
    }

    private static final int N_BODIES = 5000;
    private static final int N_ITERATIONS = 10000;
    private static final double DT = 0.01;

    private static final double BOUNDARY_X0 = -1.0;
    private static final double BOUNDARY_Y0 = -1.0;
    private static final double BOUNDARY_X1 = 1.0;
    private static final double BOUNDARY_Y1 = 1.0;

    // Maximum time in nanoseconds to wait joins
    private static final int MAX_TOTAL_JOIN_TIME_NS = 500000;
    private static final int MAX_JOIN_TIME_NS = MAX_TOTAL_JOIN_TIME_NS / 1000;
    private final int maxJoinTimePerWorker;

    // List of runnables to execute during the simulation
    public final BlockingQueue<Runnable> jobQueue;

    // The simulation can be executed with or without an interface and a logger
    private Optional<SimulationView> viewer;
    private Optional<SimulationLogger> logger;

    private SimulationState simulationState;
    private Boundary boundary;
    private CyclicBarrier barrier;
    private Semaphore semaphore;
    private ArrayList<Body> bodies;
    private List<Worker> workers;

    private int step;
    private double vt;
    private long iteration;

    public Simulator() {
        viewer = Optional.empty();
        logger = Optional.empty();
        jobQueue = new LinkedBlockingQueue<Runnable>();
        simulationState = SimulationState.READY;
        vt = 0;
        iteration = 0;

        bodies = new ArrayList<Body>(N_BODIES);

        // Calculate program threads
        int systemThreads = Runtime.getRuntime().availableProcessors();
        int programThreads = Math.max(3, (int)Math.round(systemThreads * 1.125));
        int workerThreads = Math.min(N_BODIES, programThreads - 2);
        maxJoinTimePerWorker = Math.max(MAX_JOIN_TIME_NS, MAX_TOTAL_JOIN_TIME_NS / workerThreads);

        /*------------------------------------*\
         * MANUALLY SET THE NUMBER OF WORKERS *
        \*------------------------------------*/
        //workerThreads = 4;

        // Create barrier, semaphore and boundary
        semaphore = new Semaphore(1);
        step = 0;
        barrier = new CyclicBarrier(workerThreads, () -> {
            if (step == 2) {
                // Update virtual time
                vt = vt + DT;
                iteration++;
                // Display current stage
                updateViewer();
                step = 0;
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                semaphore.release();
            } else {
                step++;
            }
        });
        boundary = new Boundary(BOUNDARY_X0, BOUNDARY_Y0, BOUNDARY_X1, BOUNDARY_Y1);

        // Calculate number of bodies and pairs per thread
        int nBodiesPerWorker = N_BODIES / workerThreads;
        int remainingBodies = N_BODIES % workerThreads;

        /*System.out.println("System threads: " + systemThreads);
        System.out.println("Program threads: " + programThreads);
        System.out.println("Worker threads: " + workerThreads + "\n");
        System.out.println("Bodies per worker: " + nBodiesPerWorker);
        System.out.println("Remaining bodies: " + remainingBodies);*/

        // Create bodies and workers
        workers = new ArrayList<Worker>(workerThreads);
        for (int i = 0, j = 0, q = 0; i < workerThreads; i++, j+=q) {
            q = i >= remainingBodies ? nBodiesPerWorker : nBodiesPerWorker + 1;
            workers.add(new Worker(bodies, j, j + q - 1, boundary, barrier, N_ITERATIONS, DT));
        }
         createBody();
    }

    // Useful to clone the simulator and launch a new simulation
    public Simulator(Simulator simulator) {
        this();
        if (simulator.viewer.isPresent()) {
            setViewer(simulator.viewer.get());
        }
        if (simulator.logger.isPresent()) {
            setLogger(simulator.logger.get());
        }
    }

    /*
       * @return the current simulation state
     */
    public synchronized SimulationState getSimulationState() {
        return simulationState;
    }

    /*
            * Set a viewer for the simulation
     * @param viewer
     */
    public void setViewer(SimulationView viewer) {
        this.viewer = Optional.of(viewer);
    }

    /*
            * Set a logger for the simulation
     * @param logger
     */
    public void setLogger(SimulationLogger logger) {
        this.logger = Optional.of(logger);
    }

    // Run a job in the job queue
    private void executeQueuedJob() {
        if (!jobQueue.isEmpty()) {
            try {
                jobQueue.take().run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // Notify the logger about a state change
    private void notifyLogger() {
        if (logger.isPresent()) {
            logger.get().update(simulationState);
        }
    }

    // Update the viewer to display the current iteration
    private void updateViewer() {
        if (viewer.isPresent()) {
            viewer.get().display(bodies, vt, iteration, boundary);
        }
    }

    // Clear the viewer's content
    private void clearViewer() {
        if (viewer.isPresent()) {
            viewer.get().display(new ArrayList<Body>(), 0, 0, boundary);
        }
    }

    public void run() {
        jobQueue.clear();
        simulationState = SimulationState.RUNNING;
        notifyLogger();
        for (Worker worker : workers) {
            worker.start();
        }
        try {
            int nTerminatedWorkers = 0;
            while (nTerminatedWorkers < workers.size()) {
                // Try joining workers
                for (Worker worker : workers) {
                    worker.join(0, maxJoinTimePerWorker);
                    if (worker.getState() == Thread.State.TERMINATED) {
                        nTerminatedWorkers++;
                    }
                }
                // Execute queued runnable
                executeQueuedJob();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (simulationState != SimulationState.CANCELED) {
            simulationState = SimulationState.COMPLETED;
        }
        notifyLogger();
        clearViewer();
    }

    /*
            * Put the simulation in PAUSED state
     */
    public synchronized void pauseSimulation() {
        jobQueue.add(() -> {
            if (simulationState != SimulationState.PAUSED) {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                simulationState = SimulationState.PAUSED;
                notifyLogger();
            }
        });
    }

    /*
            * Resume the simulation if it's in a PAUSED state
            */
    public synchronized void resumeSimulation() {
        jobQueue.add(() -> {
            if (simulationState != SimulationState.RUNNING) {
                semaphore.release();
                simulationState = SimulationState.RUNNING;
                notifyLogger();
            }
        });
    }

    /*
            * Cancel the simulation
     */
    public synchronized void cancelSimulation() {
        jobQueue.add(() -> {
            // Release semaphore if needed

            semaphore.release();
            simulationState = SimulationState.CANCELED;
            for (Worker worker : workers) {
                worker.end();
            }
        });
    }

    /*
            * Do one iteration
     */
    public synchronized void doIteration() {
        if (getSimulationState() == SimulationState.PAUSED) {
            semaphore.release();
            try {
                Thread.sleep(0, 1);
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void createBody() {
        //boundary = new Boundary(-6.0, -6.0, 6.0, 6.0);
        Random rand = new Random(System.currentTimeMillis());
        for (int i = 0; i < N_BODIES; i++) {
            double x = boundary.getX0() + rand.nextDouble() * (boundary.getX1() - boundary.getX0());
            double y = boundary.getY0() + rand.nextDouble() * (boundary.getY1() - boundary.getY0());
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), 10);
            bodies.add(b);
        }
    }

}


