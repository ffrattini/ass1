package ass1.conc.jpf;

import ass1.common.*;
import ass1.conc.Worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class Simulator extends Thread{
    /*
     * Simulates the movement of N bodies (subject to elastic hits) on a two-dimensional plane
     */
    public enum SimulationState {
        READY,
        RUNNING,
        PAUSED,
        COMPLETED,
        CANCELED
    }

    private static final int N_BODIES = 7;
    private static final int N_ITERATIONS = 10;
    private static final double BODY_MASS = 10;
    private static final double DT = 0.01;

    private static final double BOUNDARY_X0 = -1.0;
    private static final double BOUNDARY_Y0 = -1.0;
    private static final double BOUNDARY_X1 = 1.0;
    private static final double BOUNDARY_Y1 = 1.0;

    // Maximum time in nanoseconds to wait joins
    private static final int MAX_TOTAL_JOIN_TIME_NS = 500000;
    private static final int MAX_JOIN_TIME_NS = MAX_TOTAL_JOIN_TIME_NS / 1000;
    private final int maxJoinTimePerWorker;

    // List of runnables to execute during the simulation
    public final BlockingQueue<Runnable> jobQueue;

    // The simulation can be executed with or without an interface and a logger
    private Optional<SimulationView> viewer;
    private Optional<SimulationLogger> logger;

    private SimulationState simulationState;
    private Boundary boundary;
    private CyclicBarrier barrier;
    private Semaphore semaphore;
    private ArrayList<Body> bodies;
    private List<Worker> workers;

    private int step;
    private double vt;
    private long iteration;

    public Simulator() {
        viewer = Optional.empty();
        logger = Optional.empty();
        jobQueue = new LinkedBlockingQueue<Runnable>();
        simulationState = SimulationState.READY;
        vt = 0;
        iteration = 0;

        bodies = new ArrayList<Body>(N_BODIES);

        // Calculate program threads
        int systemThreads = Runtime.getRuntime().availableProcessors();
        int programThreads = Math.max(3, (int)Math.round(systemThreads * 1.125));
        int workerThreads = Math.min(N_BODIES, programThreads - 2);
        maxJoinTimePerWorker = Math.max(MAX_JOIN_TIME_NS, MAX_TOTAL_JOIN_TIME_NS / workerThreads);

        /*------------------------------------*\
         * MANUALLY SET THE NUMBER OF WORKERS *
        \*------------------------------------*/
        workerThreads = 2;

        // Create barrier, semaphore and boundary
        semaphore = new Semaphore(1);
        step = 0;
        barrier = new CyclicBarrier(workerThreads, () -> {
            if (step == 2) {
                // Update virtual time
                vt = vt + DT;
                iteration++;
                // Display current stage
                updateViewer();
                step = 0;
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                semaphore.release();
            } else {
                step++;
            }
        });
        boundary = new Boundary(BOUNDARY_X0, BOUNDARY_Y0, BOUNDARY_X1, BOUNDARY_Y1);

        // Initialize boundary and bodies
        bodies = new ArrayList<Body>();

        // This body will collide with the top and right border of the boundary
        Body b1 = new Body(1, new P2d(-0.99, 0.99), new V2d(-0.05, -0.05), BODY_MASS);

        // This body will collide with the bottom and left border of the boundary
        Body b2 = new Body(2, new P2d(0.99, -0.99), new V2d(0.05, 0.05), BODY_MASS);

        // These three bodies will collide at the same time
        Body b3 = new Body(3, new P2d(0, 0), new V2d(0.05, 0), BODY_MASS);
        Body b4 = new Body(4, new P2d(0.03, 0), new V2d(0, 0), BODY_MASS);
        Body b5 = new Body(5, new P2d(0.06, 0), new V2d(-0.05, 0), BODY_MASS);

        // These two bodies will be partially overlapped from the start
        Body b6 = new Body(6, new P2d(0, 0.5), new V2d(0.05, 0.05), BODY_MASS);
        Body b7 = new Body(7, new P2d(0.009, 0.5), new V2d(-0.05, -0.05), BODY_MASS);

        bodies.add(b1);
        bodies.add(b3);
        bodies.add(b5);
        bodies.add(b6);
        bodies.add(b2);
        bodies.add(b4);
        bodies.add(b7);

        // Create workers
        workers = new ArrayList<Worker>(workerThreads);
        workers.add(new Worker(bodies, 0, 3, boundary, barrier, N_ITERATIONS, DT));
        workers.add(new Worker(bodies, 4, 6, boundary, barrier, N_ITERATIONS, DT));
    }

    // Useful to clone the simulator and launch a new simulation
    public Simulator(Simulator simulator) {
        this();
        if (simulator.viewer.isPresent()) {
            setViewer(simulator.viewer.get());
        }
        if (simulator.logger.isPresent()) {
            setLogger(simulator.logger.get());
        }
    }

    /*
     * @return the current simulation state
     */
    public synchronized SimulationState getSimulationState() {
        return simulationState;
    }

    /*
     * Set a viewer for the simulation
     * @param viewer
     */
    public void setViewer(SimulationView viewer) {
        this.viewer = Optional.of(viewer);
    }

    /*
     * Set a logger for the simulation
     * @param logger
     */
    public void setLogger(SimulationLogger logger) {
        this.logger = Optional.of(logger);
    }

    // Run all jobs in the job queue
    private void executeQueuedJobs() {
        try {
            while(!jobQueue.isEmpty()) {
                jobQueue.take().run();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Update the viewer to display the current iteration
    private void updateViewer() {
        if (viewer.isPresent()) {
            viewer.get().display(bodies, vt, iteration, boundary);
        }
    }

    // Clear the viewer's content
    private void clearViewer() {
        if (viewer.isPresent()) {
            viewer.get().display(new ArrayList<Body>(), 0, 0, boundary);
        }
    }

    public void run() {
        jobQueue.clear();
        simulationState = SimulationState.RUNNING;
        for (Worker worker : workers) {
            worker.start();
        }
        try {
            int nTerminatedWorkers = 0;
            while (nTerminatedWorkers < workers.size()) {
                // Try joining workers
                for (Worker worker : workers) {
                    worker.join(0, maxJoinTimePerWorker);
                    if (worker.getState() == Thread.State.TERMINATED) {
                        nTerminatedWorkers++;
                    }
                }
                // Execute queued runnables
                executeQueuedJobs();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (simulationState != SimulationState.CANCELED) {
            simulationState = SimulationState.COMPLETED;
        }
        clearViewer();
    }

    /*
     * Put the simulation in PAUSED state
     */
    public synchronized void pauseSimulation() {
        jobQueue.add(() -> {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            simulationState = SimulationState.PAUSED;
        });
    }

    /*
     * Resume the simulation if it's in a PAUSED state
     */
    public synchronized void resumeSimulation() {
        jobQueue.add(() -> {
            semaphore.release();
            simulationState = SimulationState.RUNNING;
        });
    }

    /**
     * Cancel the simulation
     */
    public synchronized void cancelSimulation() {
        jobQueue.add(() -> {
            // Release semaphore if needed
            semaphore.release();
            simulationState = SimulationState.CANCELED;
            for (Worker worker : workers) {
                worker.end();
            }
        });
    }

}
