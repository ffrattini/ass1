package ass1.conc;


import ass1.common.Body;
import ass1.common.Boundary;
import ass1.common.V2d;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/*
        * Worker thread used by Simulator to run parallel operations
        */
public class Worker extends Thread {

    private final List<Body> bodies;
    private final CyclicBarrier barrier;
    private final Boundary boundary;
    private final int nIterations;
    private final double dt;
    private final int start;
    private final int stop;
    private boolean terminated = false;

    public Worker(List<Body> bodies, int start, int stop, Boundary boundary, CyclicBarrier barrier, int nIterations, double dt) {
        this.bodies = bodies;
        this.start = start;
        this.stop = stop;
        this.boundary = new Boundary(boundary);
        this.barrier = barrier;
        this.nIterations = nIterations;
        this.dt = dt;
    }

  /*
          * Stop the worker during its execution
   */
    public synchronized void end() {
        terminated = true;
    }

    public void run() {
        try {
            for (int iter = 0; iter < nIterations; iter++) {

                if (terminated) {
                    return;
                }

                System.out.println("Iteration #:" + iter);

                // Check collisions
                for (int i = start; i <= stop; i++) {
                    Body b = bodies.get(i);
                    /* compute total force on bodies */
                    V2d totalForce = computeTotalForceOnBody(b);

                    /* compute instant acceleration */
                    V2d acc = new V2d(totalForce).scalarMul(1.0 / b.getMass());

                    /* update velocity */
                    b.updateVelocity(acc, dt);
                }

                // Update position
                for (int i = start; i <= stop; i++) {
                    bodies.get(i).updatePos(dt);
                }

                barrier.await();

                // Check and solve boundary collisions
                for (int i = start; i <= stop; i++) {
                    bodies.get(i).checkAndSolveBoundaryCollision(boundary);
                }

                barrier.await();
            }
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        } catch (InterruptedException e) { }
    }


    private V2d computeTotalForceOnBody(Body b) {

        V2d totalForce = new V2d(0, 0);

        /* compute total repulsive force */

        for (int j = 0; j < bodies.size(); j++) {
            Body otherBody = bodies.get(j);
            if (!b.equals(otherBody)) {
                try {
                    V2d forceByOtherBody = b.computeRepulsiveForceBy(otherBody);
                    totalForce.sum(forceByOtherBody);
                } catch (Exception ex) {
                }
            }
        }

        /* add friction force */
        totalForce.sum(b.getCurrentFrictionForce());

        return totalForce;
    }
}