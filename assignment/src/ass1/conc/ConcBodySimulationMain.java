//package ass1.conc;
//
//import ass1.common.Chrono;
//import ass1.common.SimulationView;
//import javax.swing.*;
//
//public class ConcBodySimulationMain {
//
//    public static void main (String[] args) {
//
//        /**
//         * Concurrent with GUI
//         */
//
////        Simulator simulator = new Simulator();
////        SimulationView viewer = new SimulationView(620,620);
////        SimulationControlPanel controlPanel = new SimulationControlPanel(simulator);
////        simulator.setViewer(viewer); //si potrebbero creare due interfacce altrimenenti no
////        simulator.setLogger(controlPanel);
////        SwingUtilities.invokeLater(()-> {
////            controlPanel.setVisible(true);
////        });
//
//
//
//        /**
//         * Concurrent without GUI
//         *
//         */
//
//
//        System.out.println("ConcurrentBodySimulationMain");
//        Simulator simulator = new Simulator();
//        System.out.println("Start");
//        Chrono chrono = new Chrono();
//        chrono.start();
//        simulator.start();
//        try {
//            simulator.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        chrono.stop();
//        System.out.println("tempo impiegato :"+ chrono.getTime() + "ms");
//
//
//
//
//    }
//
//}
