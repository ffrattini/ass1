package ass1.conc;

import ass1.common.Chrono;
import ass1.common.SimulationLogger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SimulationControlPanel extends JFrame implements SimulationLogger, ActionListener  {
    private static final String BUTTON_TEXT_PLAY = "Play";
    private static final String BUTTON_TEXT_DO_ITER = "→";
    private static final String BUTTON_TEXT_PAUSE = "Pause";
    private static final String BUTTON_TEXT_RESET = "Reset";
    private static final String BUTTON_TEXT_INFO = "Info";

    private static final long serialVersionUID = 1L;

    private final JButton playPauseButton;
    private final JButton doIterationButton;
    private final JButton stopButton;
    private final JButton showInfoButton;
    private final JTextArea logArea;

    private Simulator simulator;
    private final Chrono chrono;

    public SimulationControlPanel(Simulator simulator) {
        this.simulator = simulator;
        chrono = new Chrono();

        setTitle("Simulation Control Panel");
        setSize(300, 250);
        setResizable(false);
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev) {
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev) {
                System.exit(-1);
            }
        });

        playPauseButton = new JButton(BUTTON_TEXT_PLAY);
        doIterationButton = new JButton(BUTTON_TEXT_DO_ITER);
        doIterationButton.setEnabled(false);
        stopButton = new JButton(BUTTON_TEXT_RESET);
        showInfoButton = new JButton(BUTTON_TEXT_INFO);
        logArea = new JTextArea();
        logArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(logArea);
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        btnPanel.add(playPauseButton);
        btnPanel.add(stopButton);
        btnPanel.add(showInfoButton);
        btnPanel.add(doIterationButton);
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(5, 5));
        panel.add(btnPanel, BorderLayout.SOUTH);
        panel.add(scrollPane, BorderLayout.CENTER);
        getContentPane().add(panel);
        playPauseButton.addActionListener(this);
        stopButton.addActionListener(this);
        showInfoButton.addActionListener(this);
        doIterationButton.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        Object src = ev.getSource();
        if (src == playPauseButton) {
            switch (simulator.getSimulationState()) {
                case READY:
                    simulator.start();
                    break;
                case RUNNING:
                    simulator.pauseSimulation();
                    break;
                case PAUSED:
                    simulator.resumeSimulation();
                    break;
                default:
                    break;
            }
        } else if (src == stopButton) {
            simulator.cancelSimulation();
        } else if (src == doIterationButton) {
            simulator.doIteration();
        } else {
            appendLogText("  State: " + simulator.getSimulationState() + "\n  Time: " + chrono.getTime() + "ms");
        }
    }

    // Append text to the log
    private void appendLogText(String text) {
        logArea.setText(logArea.getText() + text + "\n");
    }

    // Create a new simulator
    private void reloadSimulator() {
        simulator = new Simulator(simulator);
    }

    // Act based on the given simulation state
    public void update(Simulator.SimulationState simulationState) {
        long time;
        switch (simulationState) {
            case RUNNING:
                chrono.start();
                SwingUtilities.invokeLater(() -> {
                    appendLogText("START");
                    playPauseButton.setText(BUTTON_TEXT_PAUSE);
                    doIterationButton.setEnabled(false);
                });
                break;
            case PAUSED:
                chrono.stop();
                SwingUtilities.invokeLater(() -> {
                    appendLogText("PAUSE");
                    playPauseButton.setText(BUTTON_TEXT_PLAY);
                    doIterationButton.setEnabled(true);
                });
                break;
            case COMPLETED:
                time = chrono.getTime();
                SwingUtilities.invokeLater(() -> {
                    appendLogText("DONE\n  Simulation completed in " + time + "ms");
                    playPauseButton.setText(BUTTON_TEXT_PLAY);
                    doIterationButton.setEnabled(false);
                });
                chrono.reset();
                reloadSimulator();
                break;
            case CANCELED:
                time = chrono.getTime();
                SwingUtilities.invokeLater(() -> {
                    appendLogText("STOP\n  Simulation canceled after " + time + "ms");
                    playPauseButton.setText(BUTTON_TEXT_PLAY);
                    doIterationButton.setEnabled(false);
                });
                chrono.reset();
                reloadSimulator();
                break;
            default:
                break;
        }
    }

}
