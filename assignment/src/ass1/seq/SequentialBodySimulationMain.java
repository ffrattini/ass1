package ass1.seq;

import ass1.common.Chrono;
import ass1.common.SimulationView;

/**
 * Bodies simulation - legacy code: sequential, unstructured
 * 
 * @author aricci
 */
public class SequentialBodySimulationMain {

    public static void main(String[] args) {
        System.out.println("SequentialBodySimulation");
    	SimulationView viewer = new SimulationView(620,620);
        System.out.println("start");
        Chrono chrono = new Chrono();
        chrono.start();
    	Simulator sim = new Simulator(viewer);
        sim.executeSequential();
        chrono.stop();
        System.out.println(chrono.getTime() + " ms");

    }
}
