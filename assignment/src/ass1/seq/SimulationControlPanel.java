package ass1.seq;

import ass1.common.Chrono;
import ass1.common.SimulationLogger;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SimulationControlPanel extends JFrame  {
    private static final String BUTTON_TEXT_PLAY = "Play";
    private static final String BUTTON_TEXT_DO_ITER = "→";
    private static final String BUTTON_TEXT_RESET = "Reset";
    private static final String BUTTON_TEXT_INFO = "Info";

    private static final long serialVersionUID = 1L;

    private final JButton playPauseButton;
    private final JButton doIterationButton;
    private final JButton stopButton;
    private final JButton showInfoButton;
    private final JTextArea logArea;

    private Simulator simulator;
    private final Chrono chrono;

    public SimulationControlPanel(Simulator simulator) {
        this.simulator = simulator;
        chrono = new Chrono();

        setTitle("Simulation Control Panel");
        setSize(300, 250);
        setResizable(false);
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev) {
                System.exit(-1);
            }
            public void windowClosed(WindowEvent ev) {
                System.exit(-1);
            }
        });

        playPauseButton = new JButton(BUTTON_TEXT_PLAY);
        doIterationButton = new JButton(BUTTON_TEXT_DO_ITER);
        doIterationButton.setEnabled(false);
        stopButton = new JButton(BUTTON_TEXT_RESET);
        showInfoButton = new JButton(BUTTON_TEXT_INFO);
        logArea = new JTextArea();
        logArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(logArea);
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        btnPanel.add(playPauseButton);
        btnPanel.add(stopButton);
        btnPanel.add(showInfoButton);
        btnPanel.add(doIterationButton);
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(5, 5));
        panel.add(btnPanel, BorderLayout.SOUTH);
        panel.add(scrollPane, BorderLayout.CENTER);
        getContentPane().add(panel);

    }

}
