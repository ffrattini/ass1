package ass1.seq;

//import sun.rmi.runtime.Log;

import ass1.common.Body;
import ass1.common.V2d;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;

public class Worker extends Thread {

    private final List<Body> bodies;
    private final int nWorker;
    private final int totalWorkers;
    private final long nSteps;
    double dt;

    public Worker (String name, int number, List<Body> list, int total, long steps) {
        super(name);
        nWorker = number;
        bodies = list;
        totalWorkers = total;
        nSteps = steps;
    }
    public void run() {
        int iter = 0;
        while (iter < nSteps) {
            try {
                for (int i = (nWorker - 1) * bodies.size() / totalWorkers; i < bodies.size() / totalWorkers * nWorker; i++) {
                    Body b = bodies.get(i);

                    /* compute total force on bodies */
                    V2d totalForce = computeTotalForceOnBody(b);

                    /* compute instant acceleration */
                    V2d acc = new V2d(totalForce).scalarMul(1.0 / b.getMass());

                    /* update velocity */
                    b.updateVelocity(acc, dt);
                }
                System.out.println("await worker - " + nWorker);
                Simulator.barrier.await();

                iter++;

            } catch (InterruptedException ex) {
                System.out.println(ex);
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

    private V2d computeTotalForceOnBody(Body b) {

        V2d totalForce = new V2d(0, 0);

        /* compute total repulsive force */

        for (Body otherBody : bodies) {
            if (!b.equals(otherBody)) {
                try {
                    V2d forceByOtherBody = b.computeRepulsiveForceBy(otherBody);
                    totalForce.sum(forceByOtherBody);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        /* add friction force */
        totalForce.sum(b.getCurrentFrictionForce());

        return totalForce;
    }
}
